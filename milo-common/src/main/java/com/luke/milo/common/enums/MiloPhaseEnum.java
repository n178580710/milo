package com.luke.milo.common.enums;

import lombok.RequiredArgsConstructor;

/**
 * @Descrtption TCC阶段enum
 * @Author luke
 * @Date 2019/9/18
 **/
@RequiredArgsConstructor
public enum  MiloPhaseEnum {

    /**
     * TCC try阶段
     */
    TRYING(1,"try阶段"),

    /**
     * TCC confirm阶段
     */
    CONFIRMING(2,"confirm阶段"),

    /**
     * TCC cancel阶段
     */
    CALCELING(3,"cancel阶段");

    private final int code;

    private final String desc;

}
